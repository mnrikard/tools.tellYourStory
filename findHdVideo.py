import re
import sys
import urllib.request

sys.argv.pop(0)
storyName = " ".join(sys.argv)

lines = []
for line in sys.stdin:
    lines.append(line)

source = "\n".join(lines)

url = re.search("""(hd_src_no_ratelimit:")([^"]+)""",source).group(2)

print(url)

#urllib.request.urlopen(url).read()
urllib.request.urlretrieve(url, storyName+".mp4")
